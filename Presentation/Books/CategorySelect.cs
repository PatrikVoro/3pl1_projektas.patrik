﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.FormModels;
using Services;

namespace Presentation.Books
{
    public partial class CategorySelect : Form
    {
        public List<Category> Categories { get; set; }
        private readonly CategoryService _service;
        public CategorySelect()
        {
            InitializeComponent();
            _service = new CategoryService();
            FillGrid();
        }

        public void FillGrid()
        {
            var categories = _service.GetCategoriesNew();

            foreach (var category in categories)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["Id"].Value = category.Id.ToString();
                dataGridView1.Rows[rowIndex].Cells["CategoryName"].Value = category.Name;
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                var checkbox = (DataGridViewCheckBoxCell)((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex];

                if (checkbox.Value == null) checkbox.Value = false;

                if ((bool)checkbox.Value == false)
                {
                    checkbox.Value = true;
                }
                else
                {
                    checkbox.Value = false;
                }

                EnableDisableSelectButton();
            }

            if (e.ColumnIndex == 3)
            {
                Categories = new List<Category>();
                var category = new Category()
                {
                    Id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["Id"].Value),
                    Name = dataGridView1.Rows[e.RowIndex].Cells["CategoryName"].Value.ToString()
                };
                Categories.Add(category);
                this.Close();
            }
        }

        private void EnableDisableSelectButton()
        {
            int count = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((bool?)row.Cells["chxSelect"].Value == true)
                {
                    count++;
                }
            }

            if (count > 0)
            {
                btnSelect.Enabled = true;
            }
            else
            {
                btnSelect.Enabled = false;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            Categories = new List<Category>();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((bool?)row.Cells["chxSelect"].Value == true)
                {
                    var category = new Category()
                    {
                        Id = Convert.ToInt32(row.Cells["Id"].Value),
                        Name = row.Cells["CategoryName"].Value.ToString()
                    };

                    Categories.Add(category);
                }
            }

            this.Close();
        }
    }
}
