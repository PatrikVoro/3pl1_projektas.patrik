﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.Models;
using Services;

namespace Presentation.Categories
{
    public partial class CategoryList : Form
    {
        private readonly CategoryService _service; 
        public CategoryList()
        {
            InitializeComponent();
            _service = new CategoryService();
            FillGrid();
        }

        ~CategoryList()
        {
            _service.Dispose();
        }

        public void FillGrid()
        {
            var categories = _service.GetCategoriesNew();

            foreach (var category in categories)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["CategoryId"].Value = category.Id.ToString();
                dataGridView1.Rows[rowIndex].Cells["CategoryName"].Value = category.Name;
            }
        }

        private void BtnNewRecord_Click(object sender, EventArgs e)
        {
            CategorySave categorySaveForm = new CategorySave();
            categorySaveForm.FormClosed += CategorySaveForm_FormClosed;
            categorySaveForm.ShowDialog();
        }

        private void CategorySaveForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                var categoryId = Convert.ToInt32(((DataGridView)sender).Rows[e.RowIndex].Cells["CategoryId"].Value);
                var categoryName = ((DataGridView)sender).Rows[e.RowIndex].Cells["CategoryName"].Value.ToString();

                var category = new Dal.FormModels.Category { Id = categoryId, Name = categoryName };

                CategorySave categorySaveForm = new CategorySave(category);
                categorySaveForm.FormClosed += CategorySaveForm_FormClosed;
                categorySaveForm.ShowDialog();
            }

            if (e.ColumnIndex == 3)
            {
                var result = MessageBox.Show("Delete record?", "DeleteRecord", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    var categoryId = Convert.ToInt32(((DataGridView)sender).Rows[e.RowIndex].Cells["CategoryId"].Value);

                    _service.Delete(categoryId);
                }
            }

        }
    }
}
